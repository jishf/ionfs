﻿// Copyright Ionburst Limited 2020
using System;
using System.Collections.Generic;
using System.Text;

namespace Ionburst.Apps.IonFS
{
    public class IonFSRepositoryConfiguration
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public string DataStore { get; set; }
    }
}
