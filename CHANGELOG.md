# CHANGELOG

<!--- next entry here -->

## 0.1.1
2021-04-06

### Fixes

- dependency updates for ionfs (7aeaf35916404f255377547f01eedf2c454170b5)
- revamp ci to automate versioning and releases (74d3d85ee3d8b039661424995de06f0adc43d21a)
- add release details to ci (c0d0b08815d50a1662b2bb6cf0bcc7d806f7a5f4)
- path fixes for ci (f9bc206ef347d2626a0d9476686f78c5be3a95d5)
- final fix for windows ci (15101501465a01efc903e99c805a899d91f7d87e)
- final publish fix to attach urls to download properly (0be249f60e8ab39d1b0ce6c002eab38e4fc3cb29)

## 0.1.1
2021-04-06

### Fixes

- dependency updates for ionfs (7aeaf35916404f255377547f01eedf2c454170b5)
- revamp ci to automate versioning and releases (74d3d85ee3d8b039661424995de06f0adc43d21a)
- add release details to ci (c0d0b08815d50a1662b2bb6cf0bcc7d806f7a5f4)
- path fixes for ci (f9bc206ef347d2626a0d9476686f78c5be3a95d5)
- final fix for windows ci (15101501465a01efc903e99c805a899d91f7d87e)